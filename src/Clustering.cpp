//
// Created by beatw on 7/14/2021.
//

#include "Clustering.h"

#include "ClosestPair.h"

#include <vector>

#include <random>
#include <numeric>
#include <algorithm>
#include <optional>
#include <iostream>

namespace Clustering {

    /**
     * Initialized cluster for KMeans algorithm
     *
     * Randomly selects one of the points to be the center of each cluster
     *
     * TODO: Implement pseudo code of
     * @param pts
     * @param nbOfClusters
     * @param seed
     * @return
     */
    std::vector<Cluster> initKMeans(const std::vector<Point> &pts, size_t nbOfClusters, int seed) {
        std::vector<Cluster> clusters;

        // Chose nbOfClusters random points from pts
        std::random_device rd;
        std::default_random_engine gen(seed == -1 ? rd() : seed);
        std::uniform_int_distribution<> dis(0, pts.size() - 1);
        for (int i = 0; i < nbOfClusters; i++) {
            int index = dis(gen);
            Cluster cluster(pts[index]);
            clusters.push_back(cluster);
        }
        return clusters;
    }

    /**
     * Return the closest cluster for a given point.
     *
     * TODO: Use std::min_element to implement this function.
     *
     * @param clusters
     * @param p
     * @return
     */
    Cluster &closestCluster(std::vector<Cluster> &clusters, const Point &p) {

        // Find the closest cluster
        auto closest = std::min_element(clusters.begin(), clusters.end(), [&p](const Cluster &a, const Cluster &b) {
            return a.distance(p) < b.distance(p);
        });

        return *closest;
    }


    /**
     * Perform the KMeans clustering algorithm.
     *
     *
     * @param pts
     * @param nbOfClusters
     * @param seed
     * @return
     */
    std::vector<Cluster> kmeans(const std::vector<Point> &pts, size_t nbOfClusters, int seed) {
        std::vector<Cluster> clusters = Clustering::initKMeans(pts, nbOfClusters, seed);

        //Assign points to clusters until the clusters are stable (do not move)

        bool stable = false;
        while (!stable) {
            stable = true;
            // Assign each point to the closest cluster
            for (const Point &p: pts) {
                Cluster &cluster = Clustering::closestCluster(clusters, p);
                cluster.addPoint(p);
            }
            // Recompute the center of each cluster
            for (Cluster &c: clusters) {
                if (c.recomputeCenter()) {
                    stable = false;
                }
            }
            // Clear the clusters if not stable
            if (!stable) {
                for (Cluster &c: clusters) {
                    c.clear();
                }
            }
        }
        return clusters;
    }

    /**
     * Converts all points into clusters
     * @param pts
     * @return
     */
    std::vector<Cluster> initHierchical(const std::vector<Point> &pts) {

        std::vector<Cluster> clusters;
        for (const Point &p: pts) {
            Cluster cluster(p);
            clusters.push_back(cluster);
        }

        return clusters;
    }

    /**
     * Performs a hierarchical clustering with nbOfCluster number of clusters.
     *
     * TODO: Use ClosestPair to find the closest clusters to merge
     * @param pts
     * @param nbOfClusters
     * @return
     */
    std::vector<Cluster> hierarchical(const std::vector<Point> &pts, size_t nbOfClusters) {
        std::vector<Cluster> clusters = initHierchical(pts);

        ClosestPair<Cluster> closestPair;

        //merge two the closest clusters until there are only nbOfClusters left
        while (clusters.size() > nbOfClusters) {
            //Find the two closest clusters
            auto closest = closestPair.closestPair(clusters);

            //Merge the two clusters
            for (int i = 0; i < closest.second->getPoints().size(); i++) {
                closest.first->addPoint(
                        closest.second->getPoints()[i]);    //Add all points from second cluster to first cluster
            }

            // Recalculate center
            closest.first->recomputeCenter();

            //Remove the second cluster
            clusters.erase(std::remove(clusters.begin(), clusters.end(), *closest.second), clusters.end());

        }

        return clusters;
    }
}