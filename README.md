# TP6


## Exigences

Chaque exigence pas respectée enlève 0.1 de la note du TP.
- [X] Serveur web implémenté est fonctionnel
- [X] Les tests unitaires passent
- [X] ClosestPair utilisé pour implémenter le clustering hiérarchique
- [X] Votre repository ne doit pas contenir des fichiers binaires et temporaires (comme le répertoire
  cmake-build-debug ou .idea)
- [X] Après chaque commit, le code compile sur le CI de gitlab avec -Wall et -Werror comme options et
  les tests unitaires sont exécutés
- [X] Vous travaillez sur un fork du repo original (lien sur cyberlearn) et devez créer un merge request
  avant la deadline. 
  - Beat Wolf doit avoir accès à votre repository (ajouter comme membre ou mettre en public)